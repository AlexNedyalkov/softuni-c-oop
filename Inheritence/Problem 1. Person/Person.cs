﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Problem_1._Person
{
    public class Person
    {
        //fields
        private string name;
        private int age;

        //constructor
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }

        //properties
        public int Age
        {
            get { return this.age; }
            set { this.age = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        //methods
        public override string ToString()
        {
            StringBuilder personStringBuilder = new StringBuilder();

            personStringBuilder.Append($"Name: {this.Name}, Age: {this.Age}");

            return personStringBuilder.ToString().TrimEnd();
        }

    }
}
