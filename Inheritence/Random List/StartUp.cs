﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomRandomList

{
    public class StartUp
    {
        static void Main(string[] args)
        {

            List<string> myList = Console.ReadLine().Split().ToList();
            RandomList myRandomList = new RandomList();
            foreach (var item in myList)
            {
                myRandomList.Add(item);
            }
            string randomElement = myRandomList.RandomString();
            Console.WriteLine(randomElement);
        }
    }
}
