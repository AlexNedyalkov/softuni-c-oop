﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnotherVirtualExample
{
    public abstract class Shape
    {
        public double length;
        public double width;
        public double radius;

        //first constructor
        public Shape(double length, double width)
        {
            this.length = length;
            this.width = width;
        }

        //second constructor
        public Shape(double radius)
        {
            this.radius = radius;
        }

        //method
        public virtual void Area()
        {
            
            double area = Math.PI * Math.Pow(radius, 2);
            Console.WriteLine("Area of Shape is :{0:0.00} ", area);
        }
    }
}
