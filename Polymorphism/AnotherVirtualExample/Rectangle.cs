﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnotherVirtualExample
{
    public class Rectangle : Shape
    {
        public Rectangle(double width, double length)
            : base(length, width)
        {

        }
        
            public override void Area()
        {
            double area = length * width;
            Console.WriteLine("Area of Rectangle is :{0:0.00} ", area);
        }
    }
    
}
