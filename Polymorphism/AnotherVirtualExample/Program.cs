﻿using System;

namespace AnotherVirtualExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initiate an object with Reference Type Shape and Object Type Rectangle
            Shape myRectangle = new Rectangle(3, 5);
            myRectangle.Area();

            //Initiate an object with Reference Type Rectangle and Object Type Rectangle
            Rectangle mySecondRectangle = new Rectangle(3, 5);
            myRectangle.Area();

            //Initiate an object with Refernce Type Circle and Object Type Circle
            Circle myCircle = new Circle(1);
            myCircle.Area();

            //Initiate an object with Refernce Type Shape and Object Type Circle
            Shape mySecondCircle = new Circle(1);
            myCircle.Area();


        }
    }
}
