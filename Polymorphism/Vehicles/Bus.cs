﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles
{
    public class Bus : Vehicle
    {
        public Bus(double fuel, double liters, double tankCapacity) : base(fuel, liters, tankCapacity)
        {
        }


        //driving empty
        public override void Drive(double distance)
        {
            double fuelNeeded = distance * (this.FuelConsumption);
            if (fuelNeeded <= this.FuelQuantity)
            {
                Console.WriteLine($"Bus travelled { distance} km");
                this.FuelQuantity = this.FuelQuantity - fuelNeeded;
            }
            else
            {
                Console.WriteLine("Bus needs refueling");
            }
        }
        
        public void DriveFull(double distance)
        {
            double fuelNeeded = distance * (this.FuelConsumption + 1.4);
            if (fuelNeeded <= this.FuelQuantity)
            {
                Console.WriteLine($"Bus travelled { distance} km");
                this.FuelQuantity = this.FuelQuantity - fuelNeeded;
            }
            else
            {
                Console.WriteLine("Bus needs refueling");
            }
        }

        
    }
}
