﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles
{
    public class Truck : Vehicle
    {
        public Truck(double fuel, double liters, double tankCapacity) : base(fuel, liters, tankCapacity)
        {
        }

        public override void Drive(double distance)
        {
            double fuelNeeded = distance * (this.FuelConsumption + 1.6);
            if (fuelNeeded <= this.FuelQuantity)
            {
                Console.WriteLine($"Truck travelled { distance} km");
                this.FuelQuantity = FuelQuantity - fuelNeeded;
            }
            else
            {
                Console.WriteLine("Truck needs refueling");
            }
        }

        
    }
}
