﻿using System;
using System.Linq;

namespace Vehicles
{
    public class StartUp
    {
        static void Main(string[] args)
        {

            string[] inputDataCar = Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToArray();
            string[] inputDataTruck = Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToArray();
            string[] inputDataBus = Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToArray();

           
            Vehicle myCar = new Car(double.Parse(inputDataCar[1]), double.Parse(inputDataCar[2]), double.Parse(inputDataCar[3]));
            Vehicle myTruck = new Truck(double.Parse(inputDataTruck[1]), double.Parse(inputDataTruck[2]), double.Parse(inputDataTruck[3]));
            Bus myBus = new Bus(double.Parse(inputDataBus[1]), double.Parse(inputDataBus[2]), double.Parse(inputDataBus[3]));
            int numberOperations = int.Parse(Console.ReadLine());

            for (int i = 0; i < numberOperations; i++)
            {
                try
                {
                    string[] command = Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if (command[0] == "Drive")
                    {
                        if (command[1] == "Car")
                        {
                            myCar.Drive(double.Parse(command[2]));
                        }
                        else if (command[1] == "Truck")
                        {
                            myTruck.Drive(double.Parse(command[2]));
                        }
                        else
                        {
                            myBus.DriveFull(double.Parse(command[2]));
                        }
                    }
                    else if (command[0] == "Refuel")
                    {
                        if (command[1] == "Truck")
                        {
                            myTruck.Refuel(double.Parse(command[2]));
                        }
                        else if (command[1] == "Bus")
                        {
                            myBus.Refuel(double.Parse(command[2]));
                        }
                        else
                        {
                            myCar.Refuel(double.Parse(command[2]));
                        }
                    }

                    else
                    {
                        myBus.Drive(double.Parse(command[2]));
                    }
                }
                catch (ArgumentException ae)
                {

                    Console.WriteLine(ae.Message);
                }

            }
                Console.WriteLine($"Car: {myCar.FuelQuantity:f2}");
                Console.WriteLine($"Truck: { myTruck.FuelQuantity:f2}");
                Console.WriteLine($"Bus: { myBus.FuelQuantity:f2}");
            

            

        }
    }
}
