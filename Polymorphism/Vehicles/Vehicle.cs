﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles
{
    public abstract class Vehicle
    {
        //private fields
        private double fuelQuantity;
        private double fuel;
        private double fuelConsumption;
        private double tankCapacity;

        //Constructor
        public Vehicle(double fuel, double liters, double tankCapacity)
        {
            this.TankCapacity = tankCapacity;
            this.FuelQuantity = fuel;
            this.FuelConsumption = liters;          
        }

       

        //Properties
        public double FuelQuantity
        {
            get => this.fuelQuantity;
            protected set
            {
                if (value > this.TankCapacity)
                {
                    this.fuelQuantity = 0;
                }
                else
                {
                    this.fuelQuantity = value;
                }
                
            }
        }

        
        public double FuelConsumption
        {
            get => this.fuelConsumption;
            protected set => this.fuelConsumption = value;
        }

        public double TankCapacity
        {
            get => this.tankCapacity;
            protected set => this.tankCapacity = value;
        }



        //Methods
        public abstract void Drive(double distance);
        

        public void Refuel(double fuel)
        {
            if (fuel <= 0 )
            {
                throw new ArgumentException("Fuel must be a positive number");
            }
            else if (fuel + this.FuelQuantity > this.TankCapacity)
            {
                throw new ArgumentException($"Cannot fit {fuel} fuel in the tank");
            }
            else
            {
                this.FuelQuantity = this.FuelQuantity + fuel;
            }
           
        }

    }
}
