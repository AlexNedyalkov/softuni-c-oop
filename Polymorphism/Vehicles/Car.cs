﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles
{
    public class Car : Vehicle
    {
        //Constructor
        public Car(double fuel, double liters, double tankCapacity) : base(fuel, liters, tankCapacity)
        {
        }


        //Methods
        public override void Drive(double distance)
        {
            double fuelNeeded = distance * (this.FuelConsumption + 0.9);
            if (fuelNeeded <= this.FuelQuantity )
            {
                Console.WriteLine($"Car travelled { distance} km");
                this.FuelQuantity = this.FuelQuantity - fuelNeeded;
            }
            else
            {
                Console.WriteLine("Car needs refueling");
            }
        }

    }
}
