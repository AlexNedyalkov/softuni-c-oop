﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualExample
{
    public class Car : Vehicle
    {
        public Car(double distance, double hour, double fuel) 
            : base(distance, hour, fuel)
        {
        }

        public void AverageConsumption()
        {
            double averageConsumption = distance / fuel; ;
            Console.WriteLine($"Car AverageConsumption is {averageConsumption}");
        }

        public override void Speed()
        {
            double speed = distance / hour;
            Console.WriteLine($"Car Speed is {speed:f2}");
        }
    }
}
