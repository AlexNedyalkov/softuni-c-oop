﻿using System;

namespace VirtualExample
{
    public class StartUp
    {
        static void Main(string[] args)
        {
            //Initiate an object of the Vehicle Class
            Vehicle myVehicle = new Vehicle(360, 4, 100);
            myVehicle.AverageConsumption();
            myVehicle.Speed();

            //Initiate an object of Class Car and Reference Type Car
            Car myCar = new Car(360, 4, 100);
            myCar.AverageConsumption();
            myCar.Speed();

            //Initiate an Object of Reference type Vehicle and Object Car
            Vehicle myOtherCar = new Car(360, 4, 100);
            myOtherCar.AverageConsumption();
            myOtherCar.Speed();

        }
    }
}
