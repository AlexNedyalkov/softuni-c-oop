﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualExample
{
    public class Vehicle
    {
        //fields
        public double distance = 0.0;
        public double hour = 0.0;
        public double fuel = 0.0;

        //constrictor
        public Vehicle(double distance, double hour, double fuel)
        {
            this.distance = distance;
            this.hour = hour;
            this.fuel = fuel;
        }

        //Methods
        public void AverageConsumption()
        {
            double averageConsumption = distance / fuel; ;
            Console.WriteLine($"Vehicle AverageConsumption is {averageConsumption}");
        }

        public virtual void Speed()
        {
            double speed = distance / hour;
            Console.WriteLine($"Vehicle Speed is {speed:f2}");
        }

    }
}
