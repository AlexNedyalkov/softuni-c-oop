﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WildFarm.Foods
{
    public abstract class Food
    {
        //private fields
        private int quantity;

        //constructor
        protected Food(int quantityInput)
        {
            this.Quantity = quantityInput;
        }

        //properties
        public int Quantity
        {
            get => this.quantity;
            protected set
            {
                this.quantity = value;
            }
        }

        //
    }
}
