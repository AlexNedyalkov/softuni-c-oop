﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WildFarm.Factories
{
    using WildFarm.Foods;

    public class FoodFactory
    {
        public static Food CreateFood(string[] foodInputParameters)
        {
            Food myFood = null;

            string foodType = foodInputParameters[0];
            int quantity = int.Parse(foodInputParameters[1]);

            switch (foodType)
            {
                case "Vegetable":
                    {
                        myFood = new Vegetable(quantity);
                        break;
                    }
                case "Fruit":
                    {
                        myFood = new Fruit(quantity);
                        break;
                    }
                case "Meat":
                    {
                        myFood = new Meat(quantity);
                        break;
                    }
                case "Seeds":
                    {
                        myFood = new Seeds(quantity);
                        break;
                    }
                default:
                    break;
            }
            return myFood;
            
        }
    }
}
