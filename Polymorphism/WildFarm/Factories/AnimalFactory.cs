﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WildFarm.Factories
{
    using WildFarm.Animals;
    using WildFarm.Animals.Mammals;
    using WildFarm.Animals.Mammals.Felines;
    using WildFarm.Animals.Birds;

    public class AnimalFactory
    {
        public static Animal CreateAnimal(string[] inputAnimalParameters)
        {
            string animalType = inputAnimalParameters[0];
            string animalName = inputAnimalParameters[1];
            double animalWeight = double.Parse(inputAnimalParameters[2]);

            Animal myAnimal = null;

            switch (animalType)
            {
                case "Cat":
                    {
                        string livingRegion = inputAnimalParameters[3];
                        string breed = inputAnimalParameters[4];
                        myAnimal = new Cat(animalName, animalWeight, livingRegion, breed);
                    }
                    break;
                case "Tiger":
                    {
                        string livingRegion = inputAnimalParameters[3];
                        string breed = inputAnimalParameters[4];
                        myAnimal = new Tiger(animalName, animalWeight, livingRegion, breed);
                    }
                    break;
                case "Owl":
                    {
                        double wingSize = double.Parse(inputAnimalParameters[3]);
                        myAnimal = new Owl(animalName, animalWeight, wingSize);
                    }
                    break;
                case "Hen":
                    {
                        double wingSize = double.Parse(inputAnimalParameters[3]);

                        myAnimal = new Hen(animalName, animalWeight, wingSize);
                    }
                    break;
                case "Dog":
                    {
                        string livingRegion = inputAnimalParameters[3];

                        myAnimal = new Dog(animalName, animalWeight, livingRegion);
                    }
                    break;
                case "Mouse":
                    {
                        string livingRegion = inputAnimalParameters[3];

                        myAnimal = new Mouse(animalName, animalWeight, livingRegion);
                    }
                    break;
                default:
                    break;
            }
            return myAnimal;
        }
    }
}
