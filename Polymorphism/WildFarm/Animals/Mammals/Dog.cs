﻿using System;
using System.Collections.Generic;
using System.Text;
using WildFarm.Foods;

namespace WildFarm.Animals.Mammals
{
    using WildFarm.Foods;

    class Dog : Mammal
    {

        private const double WeightPerFood = 0.40;

        public Dog(string nameInput, double weightInput, string livingRegionInput) 
            : base(nameInput, weightInput, livingRegionInput)
        {
           
        }

        public override void Eat(Food food)
        {
            if (food.GetType().Name != nameof(Meat))
            {
                throw new ArgumentException($"{ this.GetType().Name } does not eat { food.GetType().Name}!");
            }
            this.Weight += this.Weight + food.Quantity * WeightPerFood;
            this.FoodEaten += food.Quantity;
        }

        public override string ProduceSound()
        {
            return "Woof!";
        }

        public override string ToString()
        {
            return base.ToString() + $"[{this.Name}, {this.Weight}, {this.LivingRegion}, {this.FoodEaten}]";
        }
    }
}
