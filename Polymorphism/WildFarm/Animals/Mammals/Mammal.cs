﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WildFarm.Animals.Mammals
{
    public abstract class Mammal : Animal
    {

        private string livingRegion;

        protected Mammal(string nameInput, double weightInput, string livingRegionInput)
            : base(nameInput, weightInput)
        {
            this.LivingRegion = livingRegionInput;
        }

        public string LivingRegion
        {
            get => this.livingRegion;
            protected set
            {
                this.livingRegion = value;
            }
        }

    }
}
