﻿namespace WildFarm.Animals.Mammals.Felines
{
    public abstract class Feline : Mammal
    {
        private string breed;

        protected Feline(string nameInput, double weightInput, string livingRegionInput, string breedInput) 
            : base(nameInput, weightInput, livingRegionInput)
        {
            this.Breed = breedInput;
        }

        public string Breed
        {
            get => this.breed;
            protected set
            {
                this.breed = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() +
                $"[{this.Name}, {this.Breed}, {this.Weight}, {this.LivingRegion}, {this.FoodEaten}]";
        }
    }
}
