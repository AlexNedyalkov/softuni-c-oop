﻿using System;
using System.Collections.Generic;
using System.Text;
using WildFarm.Foods;

namespace WildFarm.Animals.Mammals.Felines
{
    using WildFarm.Foods;

    public class Tiger : Feline
    {
        private const double WeightPerFood = 1.0;

        public Tiger(string nameInput, double weightInput, string livingRegionInput, string breedInput) 
            : base(nameInput, weightInput, livingRegionInput, breedInput)
        {
           
        }

        public override void Eat(Food food)
        {
            if (food.GetType().Name != nameof(Meat))
            {
                throw new ArgumentException($"{ this.GetType().Name } does not eat { food.GetType().Name}!");
            }
            this.Weight += food.Quantity * WeightPerFood;
            this.FoodEaten += food.Quantity;
        }

        public override string ProduceSound()
        {
            return "ROAR!!!";
        }
    }
}
