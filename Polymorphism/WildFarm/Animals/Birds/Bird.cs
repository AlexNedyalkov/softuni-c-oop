﻿

namespace WildFarm.Animals.Birds
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Animals;

    public abstract class Bird : Animal
    {
        private double wingSize;

        protected Bird(string nameInput, double weightInput, double wingSizeInput) 
            : base(nameInput, weightInput)
        {
            this.WingSize = wingSizeInput;
        }

        public double WingSize
        {
            get => this.wingSize;
            protected set
            {
                this.wingSize = value;
            }
        }

        //Methods
        public override string ToString()
        {
            return base.ToString() + $"[{this.Name}, {this.WingSize}, {this.Weight}, {this.FoodEaten}]";
        }
    }
}
