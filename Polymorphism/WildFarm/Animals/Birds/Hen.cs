﻿

namespace WildFarm.Animals.Birds
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using WildFarm.Animals;
    using WildFarm.Foods;

    public class Hen : Bird
    {
        private const double WeightPerFood = 0.35;

        public Hen(string nameInput, double weightInput, double wingSizeInput) 
            : base(nameInput, weightInput, wingSizeInput)
        {
           
        }

        public override void Eat(Food food)
        {
            this.Weight += food.Quantity * WeightPerFood;
            this.FoodEaten += food.Quantity;
        }


        public override string ProduceSound()
        {
            return "Cluck";
        }
    }
}
