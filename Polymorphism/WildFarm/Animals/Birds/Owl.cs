﻿

namespace WildFarm.Animals.Birds
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using WildFarm.Foods;
    

    class Owl : Bird
    {
        private const double WeightPerFood = 0.25;

        public Owl(string nameInput, double weightInput, double wingSizeInput) : 
            base(nameInput, weightInput, wingSizeInput)
        {
            
        }

        public override void Eat(Food food)
        {
            this.Weight += food.Quantity * WeightPerFood;
            this.FoodEaten += food.Quantity;
        }

        public override string ProduceSound()
        {
            return "Hoot Hoot";
        }
    }
}
