﻿

namespace WildFarm.Animals
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using WildFarm.Foods;

    public abstract class Animal
    {
        //•	Animal – string Name, double Weight, int FoodEaten;

        //private fields
        private string name;
        private double weight;
        private int foodEaten;

        //constructor
        protected Animal(string nameInput, double weightInput)
        {
            this.Name = nameInput;
            this.Weight = weightInput;
            this.FoodEaten = 0;
        }

        //properties
        public string Name
        {
            get => this.name;
            protected set
            {
                this.name = value;
            }
        }

        public double Weight
        {
            get => this.weight;
            protected set
            {
                this.weight = value;
            }
        }

        public int FoodEaten
        {
            get => this.foodEaten;
            protected set
            {
                this.foodEaten = value;
            }
        }


        //methods
        public abstract string ProduceSound();

        public abstract void Eat(Food food);

        public override string ToString()
        {
            return $"{this.GetType().Name} ";
        }

    }
}
