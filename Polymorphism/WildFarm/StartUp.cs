﻿using System;
using System.Collections.Generic;
using WildFarm.Factories;
using WildFarm.Animals;
using WildFarm.Foods;
using WildFarm;
using System.Linq;

namespace WildFarm
{
    public class StartUp
    {

        static void Main()
        {

            List<Animal> animals = new List<Animal>();

            string consoleInput = Console.ReadLine();

            while (consoleInput != "End")
            {
                string[] inputAnimalParameters = consoleInput.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                string[] foodInputParameters = Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries);

                Animal myAnimal = AnimalFactory.CreateAnimal(inputAnimalParameters);
                Food myFood = FoodFactory.CreateFood(foodInputParameters);

                Console.WriteLine(myAnimal.ProduceSound());

                try
                {
                    myAnimal.Eat(myFood);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                animals.Add(myAnimal);

                consoleInput = Console.ReadLine();
            }

            foreach (var animal in animals)
            {
                Console.WriteLine(animal);
            }
        }
    }
}
